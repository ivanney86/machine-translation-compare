CREATED BY https://smartlation.com

The following parameters should be sent to the script (as POST or GET) :

"src_txt" - Source text to be compared to a machine translation
"lang_from" - Source language code
"lang_to" - Destination language code
"dst_txt" - Translated  text to be compared to a machine translation
"token" - Token

Example:
  		"src_txt"	=>	"Today is a beautiful day. Let's go party!",
  		"lang_from"	=>  "en-us",
		"lang_to"	=>  "pt-br",
		"dst_txt"	=>  "Hoje est� um lindo dia. Vamos comemorar!",
		"token"	=>  "p+C}jK2ov]7kiuO#e&b3*q=i[z-E'd"